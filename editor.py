import sys

from PyQt5.QtWidgets import QMainWindow, QApplication, QStyleFactory
from PyQt5.QtWidgets import QFrame, QVBoxLayout
from PyQt5.QtGui import QColor
from PyQt5.Qsci import QsciScintilla, QsciLexerCustom
from lark import Lark


PLUTO_FILE = 'example.pluto'
GRAMMAR_FILE = 'grammar/pluto.lark'


STYLES = {
    0: QColor('white'),  # default style for non-keywords
    1: QColor('grey'),  # comments
    2: QColor('steelblue'),  # keywords
    3: QColor('green'),
    4: QColor('deeppink'),
    5: QColor('red'),  # comperative expressions
    6: QColor('darkorange'),  # boolean expressions
    7: QColor('yellow'),  # control flow
    8: QColor('lightblue')  # continuation_action
}

TOKEN_STYLES = {
    "NULL": 0,

    "COMMENT": 1,

    "SEMICOLON": 4,

    "PROCEDURE": 2,
    "END_PROCEDURE": 2,
    "MAIN": 2,
    "END_MAIN": 2,
    "PRECONDITIONS": 2,
    "END_PRECONDITIONS": 2,
    "DECLARE": 2,
    "END_DECLARE": 2,
    "INITIATE_AND_CONFIRM_STEP" : 2,
    "END_STEP" : 2,
    "WATCHDOG" : 2,
    "END_WATCHDOG" : 2,

    "WAIT_UNTIL": 3,
    "WAIT_FOR": 3,
    "WAIT_FOR_EVENT" : 3,
    "INITIATE_AND_CONFIRM": 3,
    "INITIATE" : 3,
    "EVENT": 3,
    "DESCRIBED_BY": 3,
    "TIMEOUT": 3,
    "RAISE_EVENT": 3,
    "WITH" : 3,
    "WITH_ARGUMENTS" : 3,
    "WITH_VALUE_SET" : 3,
    "WITH_DIRECTIVES" : 3,
    "END_WITH" : 3,
    "ARRAY" : 3,
    "END_ARRAY" : 3,
    "ENUMERATED" : 3,
    "INFORM_USER" : 3,
    "TIMEOUT" : 3,


    "BETWEEN" : 5,
    "WITHIN" : 5,
    "COMPERATIVE_AND" : 5,
    "OF" : 5,
    "IN" : 5, #in(

    "TRUE" : 6,
    "FALSE" : 6,
    "AND" : 6,
    "OR" : 6,
    "XOR" : 6,
    "NEGATION_BOOLEAN_OPERATOR" : 6,

    "IF" : 7,
    "THEN" : 7,
    "ELSE" : 7,
    "END_IF" : 7,
    "IN_CASE" : 7,
    "IS" : 7,
    "OR_IS" : 7,
    "OTHERWISE" : 7,
    "END_CASE" : 7,
    "REPEAT" : 7,
    "WHILE" : 7,
    "DO" : 7,
    "END_WHILE" : 7,
    "UNTIL" : 7,
    "FOR" : 7,
    "END_FOR" : 7,

    "CONFIRMATION" : 8,
    "END_CONFIRMATION" : 8,
    "ASK_USER" : 8,
    "MAX_TIMES" : 8,
    "TERMINATE" : 8,
    "RESUME" : 8,
    "ABORT" : 8,
    "CONTINUE" : 8,




    "LOG" : 5,

    # "SYSTEM_ELEMENT_REFERENCE" :
    # "REPORTING_DATA_REFERENCE" :
    # "UNTIL_ONE_COMPLETES" :
    # "PARAMETER_REFERENCE" :
    # "UNTIL_ALL_COMPLETE" :
    # "ACTIVITY_REFERENCE" :
    # "IN THE CONTEXT OF" :
    #
    # "UNSIGNED_INTEGER" :
    # "WITH_DIRECTIVES" :
    # "EVENT_REFERENCE" :
    #
    #
    # "SIGNED_INTEGER" :
    # "SYSTEM_ELEMENT" :
    # "REPORTING_DATA" :
    #
    # "END_PARALLEL" :
    # "SAVE_CONTEXT" :
    #
    #
    # "IN_PARALLEL" :
    #
    # "END_CONTEXT" :
    # "END_RECORD" :
    # "WITH_UNITS" :
    #
    #
    # "REFER_BY" :
    #
    # "REFER_TO" :
    #
    #
    # "SAME_AS" :
    # "OF_TYPE" :
    #
    #
    # "PARAMETER" :
    #
    # "ACTIVITY" :
    #
    #
    #
    # "VARIABLE" :
    #
    # "BOOLEAN" :
    # "CURRENT" :
    # "DEFAULT" :
    # "RESTART" :
    #
    # "EXPECT" :
    # "RECORD" :
    #
    #
    # "REAL" :
    # "BIT" :
    # "GET" :
    # "MIN" :
    # "SET" :
    # "AU":
    # "BY":
    # "DB":
    # "DO":
    # "IS":
    # "PC":
    # "TO":
}


with open(PLUTO_FILE) as f:
    PLUTO_SCRIPT = f.read()


class LexerPluto(QsciLexerCustom):

    def __init__(self, parent):
        super().__init__(parent)
        self.create_parser()
        self.create_styles()

    def language(self):
        return "Pluto"

    def description(self, style):
        return {v: k for k, v in self.token_styles.items()}.get(style, "")

    def styleText(self, start, end):
        self.startStyling(start)
        text = self.parent().text()[start:end]
        last_pos = 0

        try:
            for token in self.lark.lex(text):
                print(token.type)
                ws_len = token.pos_in_stream - last_pos
                if ws_len:
                    self.setStyling(ws_len, 0)  # whitespace

                token_len = len(bytearray(token, "utf-8"))
                self.setStyling(
                    token_len, self.token_styles.get(token.type, 0))

                last_pos = token.pos_in_stream + token_len
        except Exception as e:
            pass

    def create_styles(self):
        for style, color in STYLES.items():
            self.setColor(color, style)
            self.setPaper(QColor('#424242'), style)
            self.setFont(self.parent().font(), style)
        self.token_styles = TOKEN_STYLES

    def create_parser(self):
        with open(GRAMMAR_FILE, 'r') as f:
            grammar = f.read()
        self.lark = Lark(grammar, parser=None, lexer='standard')

    def defaultPaper(self, style):
        return QColor('#424242')


class CustomMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setGeometry(300, 300, 600, 600)
        self.setWindowTitle("PLUTO Editor")
        self.frame = QFrame(self)
        self.layout = QVBoxLayout()
        self.frame.setLayout(self.layout)
        self.setCentralWidget(self.frame)
        self.editor = QsciScintilla()
        self.lexer = LexerPluto(self.editor)
        self.editor.setLexer(self.lexer)
        self.editor.setText(PLUTO_SCRIPT)
        self.editor.setUtf8(True)
        self.layout.addWidget(self.editor)
        self.editor.setCaretForegroundColor(QColor("deeppink"))
        # self.editor.setCaretLineVisible(True)
        self.editor.setAutoIndent(True)
        self.editor.setCaretWidth(5)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    QApplication.setStyle(QStyleFactory.create('Fusion'))
    myGUI = CustomMainWindow()

    sys.exit(app.exec_())
