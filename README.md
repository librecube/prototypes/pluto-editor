# pluto-editor

This is an early prototype for a PLUTO editor.

PLUTO is a domain specific language (DSL) for writing test and operations scripts that are human-readable and machine-readable,
and thus can be used for automation of tests and operations/

Currently this prototype contains a self-standing editor written
in Python and based on QScintilla. The installation setup is
described below.

Another, perhaps preferably option would be the development of
a plug-in to the [Atom IDE](https://ide.atom.io/)

![](docs/screenshot.png)

To get started, clone the repository and install dependencies:

```
$ git clone
$ cd pluto-editor
$ virtualenv venv
$ source venv\bin\activate
$ pip install -r requirements.txt
```

To start editor:

```
$ python editor.py
```

To see the parsed terminals:

```
$ python terminals.py
```
